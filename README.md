Maven build:
```mvn clean package```

Maven test:
```mvn test```

A checkout system with configurable PromotionalRules. Default rules have been added as an example to conform to the test data given, these are:

- If you spend over £60, then you get 10% off your purchase
- If you buy 2 or more travel card holders then the price drops to £8.50.

All prices are in GBP (obviously can be changed later if needed) but are calculated using a custom Money object. This tries to simulate currency to a rough approximation for the purposes of this application. More sophisticated currency libraries were avoided for simplicity.
