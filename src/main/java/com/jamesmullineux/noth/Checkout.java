package com.jamesmullineux.noth;

import java.util.List;
import java.util.ArrayList;

public class Checkout {

    private PromotionalRules promotionalRules;
    private List<String> cart = new ArrayList<>();

    public Checkout() {
        promotionalRules = new PromotionalRules();
    }

    public Checkout(PromotionalRules promotionalRules) {
        this.promotionalRules = promotionalRules;
    }

    public void scan(String itemName) {
        cart.add(itemName);
    }

    public Money total() {
        return promotionalRules.calculate(cart);
    }

}