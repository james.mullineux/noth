package com.jamesmullineux.noth;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Money {
    
    private double price;
    private String symbol;

    public Money(double price, String symbol) {
        this.price = centsFromDollars(price);
        this.symbol = symbol;
    }

    private double centsFromDollars(double amount) {
        BigDecimal decimal = BigDecimal.valueOf(amount);
        decimal = decimal.setScale(2, RoundingMode.HALF_UP);
        decimal = decimal.multiply(new BigDecimal(100));
        return decimal.doubleValue();
    }

    public double getValue() {
        BigDecimal decimal = new BigDecimal(this.price);
        decimal = decimal.divide(new BigDecimal(100));
        return decimal.doubleValue();
    }

    public String toString() {
        double value = getValue();
        String output = Double.toString(value);
        if(output.indexOf(".") == output.length() - 2) {
            output += "0";
        }
        return symbol + output;
    }

    // sort the red black tree greatest to smallest
    public static int compare(Money m1, Money m2) {
        BigDecimal bd1 = BigDecimal.valueOf(m1.getValue());
        BigDecimal bd2 = BigDecimal.valueOf(m2.getValue());
        return bd2.compareTo(bd1);
    }

    public boolean greaterThanOrEqualTo(Money other) {
        BigDecimal bd1 = BigDecimal.valueOf(this.getValue());
        BigDecimal bd2 = BigDecimal.valueOf(other.getValue());
        if(bd1.compareTo(bd2) > 0) {
            return true;
        }
        return false;
    }

    public Money multiplyBy(double multiplier) {
        BigDecimal bd1 = BigDecimal.valueOf(this.getValue());
        BigDecimal bd2 = BigDecimal.valueOf(multiplier);
        return new Money(bd1.multiply(bd2).doubleValue(), this.symbol);
    }
}
