package com.jamesmullineux.noth;

public class Product {

    private String code;
    private String name;
    private Money price;

    public Product(String name, Money price) {
        this.name = name;
        this.price = price;
    }

    public Product(String code, String name, Money price) {
        this(name, price);
        this.code = code;
    }

    public String code() {
        return this.code;
    }

    public String name() {
        return this.name;
    }

    public Money price() {
        return this.price;
    }
}