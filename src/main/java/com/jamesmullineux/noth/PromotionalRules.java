package com.jamesmullineux.noth;

import java.util.Set;
import java.util.TreeMap;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class PromotionalRules {

    private Map<String, Money> substitutionRulesToApply;
    private Map<Money, Double> totalDiscountRulesToApply;
    private Map<String, Money> prices;

    public PromotionalRules() {
        substitutionRulesToApply = defaultSubstitutionRules();
        totalDiscountRulesToApply = defaultTotalDiscountRules();
        prices = defaultPrices();
    }

    private Map<Money, Double> defaultTotalDiscountRules() {
        Map<Money, Double> defaultRules = new HashMap<>();
        defaultRules.put(new Money(60.00, "£"), 0.9);
        return defaultRules;
    }

    private Map<String, Money> defaultSubstitutionRules() {
        Map<String, Money> defaultRules = new HashMap<>();
        defaultRules.put("001", new Money(8.50, "£"));
        return defaultRules;
    }

    private Map<String, Money> defaultPrices() {
        Map<String, Money> prices = new HashMap<>();
        prices.put("001", new Money(9.25, "£"));
        prices.put("002", new Money(45.00, "£"));
        prices.put("003", new Money(19.95, "£"));
        return prices;
    }

    public Money price(String productCode) {
        return prices.get(productCode);
    }

    public Money calculate(List<String> productCodes) {
        return applyAllRules(productCodes);
    }

    Money applyAllRules(List<String> productCodes) {
        substitutionRule(productCodes);
        Money discountedTotal = discountTotalRule(productCodes);

        return discountedTotal;
    }

    public void configureMultiBuyDiscountRules(Map<String, Money> substitutionRulesToApply) {
        this.substitutionRulesToApply = substitutionRulesToApply;
    }

    private boolean onlyDiscountRules(String code) {
        return this.substitutionRulesToApply.get(code) != null;
    }

    private void substitutionRule(List<String> productCodes) {
        if(this.substitutionRulesToApply.size() > 0) {
            long count = productCodes.stream().filter(this::onlyDiscountRules).count();
            if(count > 1) {
                Set<String> uniques = productCodes.stream().collect(Collectors.toSet());
                uniques.stream()
                    .filter(this::onlyDiscountRules)
                    .forEach(code -> prices.replace(code, this.substitutionRulesToApply.get(code)));
            }
        }
    }

    public void configureTotalDiscountRules(Map<Money, Double> totalDiscountRulesToApply) {
        this.totalDiscountRulesToApply = new TreeMap<Money, Double>(Money::compare);
        this.totalDiscountRulesToApply.putAll(totalDiscountRulesToApply);
    }

    private Money discountTotalRule(List<String> productCodes) {
        double total = productCodes.stream()
            .map((code)->prices.get(code).getValue())
            .reduce(0.0, Double::sum);
        Money discountedTotal = new Money(total, "£");

        for(Map.Entry<Money, Double> entry : this.totalDiscountRulesToApply.entrySet()) {
            if(discountedTotal.greaterThanOrEqualTo(entry.getKey())) {
                discountedTotal = discountedTotal.multiplyBy(entry.getValue());
            }
        }
        return discountedTotal;
    }

}