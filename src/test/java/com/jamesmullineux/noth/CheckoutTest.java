package com.jamesmullineux.noth;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CheckoutTest {

    @Test
    public void testCheckoutScanAndTotal() {
        String productCode = "001";
        Checkout co = new Checkout();
        co.scan(productCode);
        Money price = co.total();
        assertThat(price.getValue()).isEqualTo(9.25);
    }

    @Test
    public void testCheckoutScanMulitpleItemsAndTotal() {
        String productCode1 = "001";
        String productCode2 = "002";
        Checkout co = new Checkout();
        co.scan(productCode1);
        co.scan(productCode2);
        Money price = co.total();
        assertThat(price.getValue()).isEqualTo(9.25 + 45.0);
    }

    @Test
    public void testCheckoutScanProductCode001() {
        String productCode = "001";
        Checkout co = new Checkout();
        co.scan(productCode);
        Money price = co.total();
        assertThat(price.getValue()).isEqualTo(9.25);
    }

    @Test
    public void testCheckoutScanProductCode002() {
        String productCode = "002";
        Checkout co = new Checkout();
        co.scan(productCode);
        Money price = co.total();
        assertThat(price.getValue()).isEqualTo(45.00);
    }

    @Test
    public void testCheckoutScanProductCode003() {
        String productCode = "003";
        Checkout co = new Checkout();
        co.scan(productCode);
        Money price = co.total();
        assertThat(price.getValue()).isEqualTo(19.95);
    }

    @Test
    public void testPromotionRulesAppliesSubstitutionRule() {
        String productCode = "001";
        double lowerPrice = 8.50;
        PromotionalRules rules = new PromotionalRules();
        Checkout co = new Checkout(rules);
        co.scan(productCode);
        co.scan(productCode);
        Money price = co.total();
        assertThat(price.getValue()).isEqualTo(lowerPrice * 2);
    }
    
    @Test
    public void testMultipleSubstitutionRulesCanBeApplied() {
        String productCode001 = "001";
        double lowerPrice001 = 8.50;
        String productCode003 = "003";
        double lowerPrice003 = 9.95;
        PromotionalRules rules = new PromotionalRules();
        HashMap<String, Money> discountRules = new HashMap<>();
        discountRules.put("001", new Money(lowerPrice001, "£"));
        discountRules.put("003", new Money(lowerPrice003, "£"));
        rules.configureMultiBuyDiscountRules(discountRules);
        Checkout co = new Checkout(rules);
        co.scan(productCode001);
        co.scan(productCode001);
        co.scan(productCode003);
        co.scan(productCode003);
        Money price = co.total();
        assertThat(price.getValue()).isEqualTo((lowerPrice001 * 2) + (lowerPrice003 * 2));
    }

    @Test
    public void testPromotionRulesAppliesDiscountTotalRule() {
        String productCode = "002";
        double lowerPrice = 45.0 * 0.9;
        PromotionalRules rules = new PromotionalRules();
        Checkout co = new Checkout(rules);
        co.scan(productCode);
        co.scan(productCode);
        Money price = co.total();
        assertThat(price.getValue()).isEqualTo(lowerPrice * 2);
    }

    @Test
    public void testPromotionRulesCanApplyMultipleDiscountTotalRules() {
        String productCode = "002";
        double lowerPrice = 45.0 * 0.9 * 0.5;
        PromotionalRules rules = new PromotionalRules();
        Map<Money, Double> discountTotalRules = new HashMap<>();
        discountTotalRules.put(new Money(60.00, "£"), 0.9);
        discountTotalRules.put(new Money(10.00, "£"), 0.5);
        rules.configureTotalDiscountRules(discountTotalRules);
        Checkout co = new Checkout(rules);
        co.scan(productCode);
        co.scan(productCode);
        Money price = co.total();
        assertThat(price.getValue()).isEqualTo(lowerPrice * 2);
    }
}
