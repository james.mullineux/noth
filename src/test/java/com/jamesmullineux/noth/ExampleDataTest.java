package com.jamesmullineux.noth;

import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ExampleDataTest {

    @Test
    public void testData1() {
        double expectedPrice = 66.78;
        PromotionalRules rules = new PromotionalRules();
        Checkout co = new Checkout(rules);
        co.scan("001");
        co.scan("002");
        co.scan("003");
        Money price = co.total();
        assertThat(price.getValue()).isEqualTo(expectedPrice);
    }

    @Test
    public void testData2() {
        double expectedPrice = 36.95;
        PromotionalRules rules = new PromotionalRules();
        Checkout co = new Checkout(rules);
        co.scan("001");
        co.scan("003");
        co.scan("001");
        Money price = co.total();
        assertThat(price.getValue()).isEqualTo(expectedPrice);
    }

    @Test
    public void testData3() {
        double expectedPrice = 73.76;
        PromotionalRules rules = new PromotionalRules();
        Checkout co = new Checkout(rules);
        co.scan("001");
        co.scan("002");
        co.scan("001");
        co.scan("003");
        Money price = co.total();
        assertThat(price.getValue()).isEqualTo(expectedPrice);
    }

}
