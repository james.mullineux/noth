package com.jamesmullineux.noth;

import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class MoneyTest {

    @Test
    public void testMoneyHasPriceAndCurrency() {
        Money price = new Money(9.95, "£");
        assertThat(price.toString()).isEqualTo("£9.95");
    }

    @Test
    public void testMoneyRoundsToTwoDecimalPlaces() {
        Money price = new Money(100.219999, "£");
        assertThat(price.toString()).isEqualTo("£100.22");
    }

    @Test
    public void testMoneyAmountsAreRoundedUp() {
        Money price = new Money(99.999, "£");
        assertThat(price.toString()).isEqualTo("£100.00");
    }

    @Test
    public void testMoneyAmountsAreRoundedHalfUp() {
        Money price = new Money(99.995, "£");
        assertThat(price.toString()).isEqualTo("£100.00");
    }

    @Test
    public void testMoneyAmountsAreRoundedDownUp() {
        Money price = new Money(99.994, "£");
        assertThat(price.toString()).isEqualTo("£99.99");
    }

    @Test
    public void testMoneyAmountsRetainTwoDecimalPlacesWhenTheyAreZeros() {
        Money price = new Money(0.00, "£");
        assertThat(price.toString()).isEqualTo("£0.00");
    }

}