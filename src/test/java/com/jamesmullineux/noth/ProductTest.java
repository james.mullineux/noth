package com.jamesmullineux.noth;

import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ProductTest {

    @Test
    public void testProductsHaveNames() {
        String name = "Travel Card Holder";
        Money price = new Money(9.95, "£");
        Product product = new Product(name, price);
        assertThat(product.name()).isEqualTo(name);
    }

    @Test
    public void testProductsHavePrices() {
        String name = "Travel Card Holder";
        Money price = new Money(9.95, "£");
        Product product = new Product(name, price);
        assertThat(product.price()).isEqualTo(price);
    }

    @Test
    public void testProductsHaveCode() {
        String code = "001";
        String name = "Travel Card Holder";
        Money price = new Money(9.95, "£");
        Product product = new Product(code, name, price);
        assertThat(product.code()).isEqualTo(code);
    }

}