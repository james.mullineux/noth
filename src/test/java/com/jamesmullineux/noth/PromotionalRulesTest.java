package com.jamesmullineux.noth;

import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;

import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class PromotionalRulesTest {

    @Test
    public void testPromotionRulesContainsMapWithProduct1() {
        String productCode = "001";
        Money price = new Money(9.25, "£");
        PromotionalRules promotionalRules = new PromotionalRules();
        assertThat(promotionalRules.price(productCode).getValue()).isEqualTo(price.getValue());
   }

   @Test
   public void testPromotionRulesContainsMapWithProduct2() {
       String productCode = "002";
       Money price = new Money(45.00, "£");
       PromotionalRules promotionalRules = new PromotionalRules();
       assertThat(promotionalRules.price(productCode).getValue()).isEqualTo(price.getValue());
    }

    @Test
    public void testPromotionRulesContainsMapWithProduct3() {
        String productCode = "003";
        Money price = new Money(19.95, "£");
        PromotionalRules promotionalRules = new PromotionalRules();
        assertThat(promotionalRules.price(productCode).getValue()).isEqualTo(price.getValue());
    }

    @Test
    public void testPromotionRulesCanHaveMultipleSubstitutionRulesConfigured() {
        PromotionalRules rules = new PromotionalRules();
        HashMap<String, Money> discountRules = new HashMap<>();
        Money lowerPrice001 = new Money(8.50, "£");
        Money lowerPrice002 = new Money(43.00, "£");
        discountRules.put("001", lowerPrice001);
        discountRules.put("002", lowerPrice002);
        rules.configureMultiBuyDiscountRules(discountRules);
        rules.calculate(Arrays.asList("001", "001", "002", "002"));
        assertThat(rules.price("001")).isEqualTo(lowerPrice001);
        assertThat(rules.price("002")).isEqualTo(lowerPrice002);
    }

    @Test
    public void testPromotionRulesCanHaveMultipleDiscountTotalRulesConfigured() {
        double lowerPrice = 45.0 * 0.9 * 0.5;

        PromotionalRules rules = new PromotionalRules();
        Map<Money, Double> discountTotalRules = new HashMap<>();
        discountTotalRules.put(new Money(60.00, "£"), 0.9);
        discountTotalRules.put(new Money(10.00, "£"), 0.5);
        rules.configureTotalDiscountRules(discountTotalRules);

        Money result = rules.applyAllRules(Arrays.asList("002", "002"));
        assertThat(result.getValue()).isEqualTo(lowerPrice * 2);
    }

    @Test
    public void testMultipleDiscountTotalRulesCanBeConfiguredInAnyOrder() {
        double lowerPrice = 45.0 * 0.9 * 0.5;

        PromotionalRules rules = new PromotionalRules();
        Map<Money, Double> discountTotalRules = new HashMap<>();
        discountTotalRules.put(new Money(10.00, "£"), 0.5);
        discountTotalRules.put(new Money(60.00, "£"), 0.9);
        rules.configureTotalDiscountRules(discountTotalRules);

        Money result = rules.applyAllRules(Arrays.asList("002", "002"));
        assertThat(result.getValue()).isEqualTo(lowerPrice * 2);
    }

}
